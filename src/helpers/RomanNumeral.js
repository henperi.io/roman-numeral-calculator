export const romanLookup = {
  M: 1000,
  CM: 900,
  D: 500,
  CD: 400,
  C: 100,
  XC: 90,
  L: 50,
  XL: 40,
  X: 10,
  IX: 9,
  V: 5,
  IV: 4,
  I: 1,
};

export class RomanNumerals {
  static toRoman(number) {
    let result = "";

    Object.keys(romanLookup).forEach((key) => {
      while (number >= romanLookup[key]) {
        result += key;
        number -= romanLookup[key];
      }
    });

    return result;
  }

  static fromRoman(numerals) {
    let result = 0;

    const splitNumerals = numerals.split("");

    for (
      let currentIndex = 0;
      currentIndex < splitNumerals.length;
      currentIndex += 1
    ) {
      const nextIndex = currentIndex + 1;

      const current = romanLookup[splitNumerals[currentIndex]];
      const next = romanLookup[splitNumerals[nextIndex]];

      if (!next || current >= next) {
        result += romanLookup[splitNumerals[currentIndex]];
      } else {
        result -= romanLookup[splitNumerals[currentIndex]];
      }
    }

    return result;
  }
}
