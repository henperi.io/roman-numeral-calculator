export const isValidNumber = (number) => {
  return /^[0-9]+$/.test(number);
};
