import { romanLookup } from "./RomanNumeral";

export const isValidNumeral = (str) => {
  if (!str) return false;
  const romanNumerals = Object.keys(romanLookup);
  const chars = str.split("");

  const invalidChars = [];

  chars.forEach((char) => {
    if (!romanNumerals.includes(char)) {
      invalidChars.push(char);
    }
  });

  return invalidChars.length === 0;
};
