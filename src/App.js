import { AppLayout } from "./components/AppLayout";
import { ConvertCard } from "./components/ConvertCard";

function App() {
  return (
    <AppLayout>
      <ConvertCard conversionType="toRoman" />

      <ConvertCard conversionType="fromRoman" />
    </AppLayout>
  );
}

export default App;
