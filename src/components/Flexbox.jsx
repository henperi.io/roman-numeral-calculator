import styled from "styled-components";

// type FlexProperties = Pick<
//   React.CSSProperties,
//   | "justifyContent"
//   | "alignContent"
//   | "alignItems"
//   | "flexFlow"
//   | "flexWrap"
//   | "flexDirection"
//   | "gap"
// >;

// https://styled-components.com/docs/advanced#style-objects
export const Flexbox = styled.div(
  ({
    justifyContent,
    alignContent,
    alignItems,
    flexFlow,
    flexWrap,
    flexDirection,
    gap,
  }) => ({
    display: "flex",
    justifyContent,
    alignContent,
    alignItems,
    flexFlow,
    flexWrap,
    flexDirection,
    gap,
  })
);
