import styled from "styled-components";
import { css } from "styled-components";

// type Props = {error: boolean; header: boolean}
export const Text = styled.span`
  ${(props) =>
    props.error &&
    css`
      color: red;
    `}

  ${({ header }) =>
    header &&
    css`
      font-size: 18px;
      font-weight: 600;
    `}
`;
