import { Flexbox } from "./Flexbox";
import styled from "styled-components";

export const ConversionBox = styled(Flexbox)`
  background: #f0f7fe;
  border: 1px solid #eee;
  min-width: 50%;
  height: 16px;
  padding: 1rem;
  text-align: center;
  justify-content: center;
`;
