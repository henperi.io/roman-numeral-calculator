// import { css } from "@emotion/react";
import styled from "styled-components";
import { css } from "styled-components";

// type Props = {hasError: boolean}
export const InputField = styled.input`
  display: flex;
  padding: 0.75rem 1.25rem;
  border: 1px solid lightGrey;
  width: 100%;
  font-size: 16px;
  outline: none;
  margin-top: 2px;
  border-radius: 33.5px;

  background: #f0f7fe;

  ${(props) =>
    props.hasError &&
    css`
      border: 1px solid red;
      &:focus {
        box-shadow: unset;
        border-color: red;
      }
    `};

  ::placeholder {
    color: #716f6f;
  }

  :disabled {
  }
`;
