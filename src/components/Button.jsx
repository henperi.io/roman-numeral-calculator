import styled from "styled-components";
import { css } from "styled-components";

export const Button = styled.button`
  border-radius: 50%;
  padding: 20px 40px;

  ${({ noPadding }) =>
    noPadding &&
    css`
      padding: 0px;
    `}

  outline: none;
  border: none;
  min-width: 28px;
  min-height: 28px;
  cursor: pointer;
  box-shadow: 0px 8px 22px rgba(41, 39, 58, 0.21);

  color: #fff;
  font-weight: bold;
  font-weight: 600;

  display: flex;
  justify-content: center;
  align-items: center;

  background: linear-gradient(139.54deg, #828282 5.54%, #323f4b 93.58%);

  ${({ variant }) =>
    variant === "white" &&
    css`
      background: #ffffff;
      color: black;
      box-shadow: 0px 100px 80px rgba(169, 168, 177, 0.07),
        -3px 12px 17.869px rgba(99, 96, 131, 0.09);
    `}

  border-radius: 30px;

  &:hover {
    box-shadow: 0px 8px 22px 1px rgba(41, 39, 58, 0.41);
  }

`;
