import styled from "styled-components";
import { Flexbox } from "./Flexbox";

export const AppLayout = styled(Flexbox).attrs({
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
})`
  box-sizing: border-box;
  width: 100%;
  border: solid #5b6dcd 10px;
  padding: 5px;

  background: #f1fafd;
  box-shadow: 0px 4px 8px 1px rgba(0, 0, 0, 0.05);
  padding: 1rem 2rem;
  min-height: 100vh;
`;
