import styled from "styled-components";
import { Flexbox } from "./Flexbox";

export const Card = styled(Flexbox)`
  background: #fff;
  box-shadow: 0px 4px 8px 1px rgba(0, 0, 0, 0.05);
  padding: 2rem 4rem;
  margin: 2rem;
  min-width: 50%;
  text-align: center;

  * {
      margin-bottom: 0.5rem;
  }
`;
