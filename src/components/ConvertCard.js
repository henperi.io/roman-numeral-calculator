import { useState } from "react";
import { isValidNumber } from "../helpers/isValidNumber";
import { isValidNumeral } from "../helpers/isValidNumeral";
import { RomanNumerals } from "../helpers/RomanNumeral";
import { Button } from "./Button";
import { Card } from "./Card";
import { ConversionBox } from "./ConversionBox";
import { InputField } from "./InputField";
import { Text } from "./Text";

// type conversionType = 'toRoman' | 'fromRoman'
export const ConvertCard = ({ conversionType }) => {
  const [value, setValue] = useState("");
  const [result, setResult] = useState("");
  const [error, setError] = useState("");

  const clearPreviousMessages = () => {
    setError("");
    setResult("");
  };

  const handleConvert = () => {
    if (conversionType === "toRoman") {
      if (isValidNumber(value)) {
        setResult(RomanNumerals.toRoman(value));
      } else {
        setError("Is not a valid number");
      }
    } else {
      const newValue = value.toUpperCase();
      if (isValidNumeral(newValue)) {
        setResult(RomanNumerals.fromRoman(newValue));
      } else {
        setError("Is not a valid roman numeral");
      }
    }
  };

  return (
    <>
      <Card
        as="form"
        onSubmit={(e) => {
          e.preventDefault();
          handleConvert(value);
        }}
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        {conversionType === "toRoman" ? (
          <Text header>Convert Number to Roman Numeral</Text>
        ) : (
          <Text header>Convert Roman Numeral to Number</Text>
        )}

        <InputField
          value={value}
          onChange={(e) => {
            setValue(e.target.value);
            clearPreviousMessages();
          }}
          type={conversionType === "toRoman" ? "number" : "text"}
          placeholder={
            conversionType === "toRoman"
              ? "Enter Number to convert"
              : "Enter Roman Numeral to convert"
          }
          hasError={!!error}
        />
        {error && <Text error={error}>{error}</Text>}

        <div>
          <Button type="submit">Submit</Button>
        </div>
        <Text>Result</Text>
        <ConversionBox>{result}</ConversionBox>
      </Card>
    </>
  );
};
